# RGCode 0.1.1
## New features
- Added a more in-depth installation and usage guide, which you can find [here](./Tutorial_RGCode.pdf).
## Bug fixes
- Fixed a bug during installation.
# RGCode 0.1
Initial release as published in [Masin and Claes et al. (2021)](https://www.nature.com/articles/s41598-020-80308-y).